/**
 * Created by tony on 01.12.16.
 */
public class Polindrom {
    public static void main(String[] args) {
        String word = "kaz  an";



        String word_clear = word.replaceAll("\\s+","");

        String rev = new StringBuilder(word_clear).reverse().toString();

        System.out.println(word_clear + '\t');
        System.out.println(rev);

        if (word_clear.equals(rev)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }

    }
}
