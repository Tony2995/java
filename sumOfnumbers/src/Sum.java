/**
 * Created by tony on 25.11.16.
 */

import org.omg.PortableInterceptor.INACTIVE;

import java.util.Scanner;
import java.util.StringJoiner;


public class Sum {

    public static void main(String[] args) {
        //Scanner reader = new Scanner(System.in);

        System.out.println("Enter a number: ");
        //int n = reader.nextInt();

        int n = 1234;
        int sum = 0;

        while (n != 0) {
            sum = sum + (n % 10);
            n = n / 10;
        }

    }
}
