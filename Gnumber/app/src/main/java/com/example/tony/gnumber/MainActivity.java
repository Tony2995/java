package com.example.tony.gnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int max;
    int min;
    int avg;
    int status = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void run(View v) {

        TextView from = (TextView) findViewById(R.id.from);
        TextView to = (TextView) findViewById(R.id.to);

        min = Integer.parseInt(from.getText().toString());
        max = Integer.parseInt(to.getText().toString());
        avg = (max + min) / 2;

        setContentView(R.layout.activity_main_2);

        TextView range = (TextView) findViewById(R.id.info);
        TextView question = (TextView) findViewById(R.id.textQuestion);

        range.setText("Загадайте число от " + Integer.toString(min) + " до " + Integer.toString(max));
        question.setText("Ваше число >= " + Integer.toString(avg) + " ?");
    }

    protected void newGame(View v) {
        min = 0;
        max = 0;
        avg = 0;
        status = 1;

        setContentView(R.layout.activity_main);
    }

    protected int onClick(View v) {

        TextView question = (TextView) findViewById(R.id.textQuestion);

        if (max == min) {
            question.setText("Ответ: " + Integer.toString(min));
            return 0;
        }

        if (status == 0) {
            if (v.getId() == R.id.yes) {
                if (avg < 0) {
                    avg--;
                    question.setText("Ответ: " + Integer.toString(avg));
                } else {
                    question.setText("Ответ: " + Integer.toString(avg));
                }

            } else {
                question.setText("Ответ: " + Integer.toString(max));
            }

            return 0;

        } else {

            if (v.getId() == R.id.yes) {
                min = avg;
                avg = (max + min) / 2;
            } else {
                max = avg;
                avg = (max + min) / 2;
            }
        }

        if (max - min == 1) {
            status = 0;
            question.setText("Ваше число < " + Integer.toString(max));
        } else {
            status = 1;
            question.setText("Ваше число >= " + Integer.toString(avg) + " ?");

        }

        return 0;
    }

}
